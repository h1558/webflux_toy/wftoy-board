package com.hans.wftoyboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WftoyBoardApplication {

    public static void main(String[] args) {
        SpringApplication.run(WftoyBoardApplication.class, args);
    }

}
